<?php

abstract class Entity { // Существа
}

interface getNameAnimalInterface {  //получить название животного
    public function getNameAnimal(): String;
}

interface getNameProductInterface { //получить названите продукта (Яйцо / молоко / мясо и т.д.)
    public function getNameProduct(): String;
}

interface getQtyProductInterface { // получить кол-во продукта
    public function getQtyProduct(): int;
}

class Animal extends Entity implements getNameAnimalInterface, getNameProductInterface, getQtyProductInterface { // Животное
    private $id; // номер животного
    private $nameAnimal; // имя животного
    private $nameProduct; // имя продукта
    private $qtyProduct; // кол-во продукта


    public function __construct(String $nameAnimal, String $nameProduct, int $qtyProduct) {
        $this->id = md5(rand());
        $this->nameAnimal = $nameAnimal;
        $this->nameProduct = $nameProduct;
        $this->qtyProduct = $qtyProduct;
    }

    public function getNameAnimal(): string
    {
        return $this->nameAnimal;
    }

    public function getNameProduct(): string
    {
        return $this->nameProduct;
    }

    public function getQtyProduct(): int
    {
        return $this->qtyProduct;
    }
}

abstract class Building {
    private $nameBuilding; // название строения
    private $typeBuilding; // тип строения
    private $numberFloors; // нормер строения

    public function __construct(String $nameBuilding, String $typeBuilding, int $numberFloors) {
        $this->nameBuilding = $nameBuilding;
        $this->typeBuilding = $typeBuilding;
        $this->numberFloors = $numberFloors;
    }

    public function getNameBuilding() // получить имя строения
    {
        return $this->nameBuilding;
    }

    public function getTypeBuilding() // получить тип строения
    {
        return $this->typeBuilding;
    }

    public function getNumberFloors() // получить номер строения
    {
        return $this->numberFloors;
    }
}

interface SaveProduct { // кол-во хранения продукции
    public function getLimitProduct();
    public function setLimitStorageProduct();
    public function setStorageProduct(int $qtyProduct);
    public function getTotalNumberOfProduct();
    public function setTotalNumberOfProduct(int $typeQtyProduct);
}

class Barn extends Building implements SaveProduct { //амбар
    private $limitProduct = 0;
    private $typeQtyProduct = 0;

    public function __construct(string $nameBuilding, string $typeBuilding, int $numberFloors, int $limitProduct)
    {
        parent::__construct($nameBuilding, $typeBuilding, $numberFloors);
        $this->limitProduct = $limitProduct;
    }

    public function getLimitProduct() // получить макс кол-во продукции
    {
        return $this->limitProduct;
    }

    public function setLimitStorageProduct() {
        $this->limitProduct = 0;
    }

    public function setStorageProduct(int $qtyProduct)
    {
        $this->limitProduct -= $qtyProduct;
    }

    public function getTotalNumberOfProduct()
    {
        return $this->typeQtyProduct;
    }

    public function setTotalNumberOfProduct(int $typeQtyProduct)
    {
        $this->typeQtyProduct = $typeQtyProduct;
    }
}

interface FarmStorageInterface {
    public function collectProducts(String $nameAnimal);
    public function howMuchProduct(): int;
    public function howMuchProductAnimal(): int;
    public function getAddProductFarmInBarn(int $qtyProduct);
}

class Farm extends Building implements FarmStorageInterface
{
    private $storage;
    private $animals = []; // массив для животных

    public function __construct(string $nameBuilding, string $typeBuilding, int $numberFloors, SaveProduct $storage)
    {
        parent::__construct($nameBuilding, $typeBuilding, $numberFloors);
        $this->storage = $storage;
    }

    public function addAnimal(Animal $animal) {
        $this->animals[] = $animal;
    }

    public function collectProducts(String $nameAnimal) // добавить продукт
    {
        $qtyProduct = 0;
        foreach ($this->animals as $animal) {
            if ($animal->getNameAnimal() == $nameAnimal) {
                $qtyProduct += $animal->getQtyProduct();
            }
        }
        $this->storage->setTotalNumberOfProduct($qtyProduct);
        $this->getAddProductFarmInBarn($qtyProduct);
    }

    public function getAddProductFarmInBarn(int $qtyProduct) {
        $limitProduct = $this->storage->getLimitProduct();
        if ($limitProduct === 0) {
            return;
        }

        if ($limitProduct < $qtyProduct) {
            $this->storage->setLimitStorageProduct();
            return;
        }
        $this->storage->setStorageProduct($qtyProduct);
    }

    public function howMuchProduct(): int // как много продукта
    {
        return $this->storage->getLimitProduct();
    }

    public function howMuchProductAnimal(): int
    {
        return $this->storage->getTotalNumberOfProduct();
    }
}

$barn = new Barn("НовыйАмбар7000", "Амбар", 5, 7000);
$farm = new Farm("FastFarm", "Farm", 7, $barn);

for ($i = 0; $i < 70; $i++) {
    $farm->addAnimal(new Animal("Корова", "Молоко", rand(8, 12)));
}

for ($i = 0; $i < 20; $i++) {
    $farm->addAnimal(new Animal("Курица", "Яйца", rand(0, 1)));
}

for ($i = 0; $i < 20; $i++) {
    $farm->addAnimal(new Animal("Коза", "Молоко", rand(3, 8)));
}

$farm->collectProducts("Курица");
echo "Кол-во продукции курицы = ".$farm->howMuchProductAnimal()."\n";
echo "---------------------------------------\n";
$farm->collectProducts("Корова");
echo "Кол-во продукции Коровы = ".$farm->howMuchProductAnimal()."\n";
echo "---------------------------------------\n";
$farm->collectProducts("Коза");
echo "Кол-во продукции Козы = ".$farm->howMuchProductAnimal()."\n";
echo "---------------------------------------\n";
echo "Кол-во свободного места в амбаре = ".$farm->howMuchProduct()."\n";
